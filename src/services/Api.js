import axios from 'axios'

const API = axios.create({
  baseURL: 'http://localhost:3000/api',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})
export default {
  async signup (newUser) {
    const response = await API.post('/auth/signup', {
      ...newUser
    })
    return response.data
  },
  async login (user) {
    const response = await API.post('/auth/login', {
      ...user
    })
    return response.data
  },
  async getWallpaper () {
    const response = await API.get('/wallpaper')
    return response.data
  },
  async addFavourite (wallpaperId) {
    const response = await API.post(`users/me/favourites/${wallpaperId}`, {}, {
      headers: {
        'token': localStorage.getItem('token')
      }
    })
    return response.data
  },
  async showFavouriteWallpaper () {
    const response = await API.get('users/me/favourites', {
      headers: {
        'token': localStorage.getItem('token')
      }
    })
    return response.data
  }
}
