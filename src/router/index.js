import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Signup from '../views/Signup.vue'
import Login from '../views/Login.vue'
import Favoritos from '../views/Favoritos.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/favoritos',
    name: 'Favoritos',
    component: Favoritos,
    beforeEnter (to, from, next) {
      if (!localStorage.getItem('token')) {
        next({ name: 'Login' })
      }
      next()
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: 'http://localhost:3000/api',
  routes
})

export default router
